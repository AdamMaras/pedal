# PEDAL - Prototype Engineering Drawing and Automation Language

PEDAL is a prototype project, created to explore the concept of a computer language and tooling suite built to define geometry using parametric modeling, constraints, and composition.

**This project is not intended to be used for production purposes, and will not be maintained as such.** If this prototype yields a successful and practical model, a new project (with design documentation, a planning workflow, contribution guidelines, etc.) will be created to supersede this one.

## Areas of exploration

While PEDAL doesn't have any specific design goals, it is intended to explore several areas within the domain of code-based CAD/CAM:

- **Language design:** building and validating a declarative computer language that works well for defining geometry and constraints, describing geometric operations and mathematical expressions, and writing documentation
- **The mathematics behind parametric design:** defining CAD/CAM operations in mathematical terms and building minimally-viable infrastructure to do the high-level math needed to create, constrain, and validate geometry
- **Debugging:** creating a minimally-viable workflow to prove the concept of debugging the creation of a component
- **Automated testing and validation:** identifying opportunities to apply the concept of unit testing to geometric components
- **Tooling ergonomics and continuous integration:** validating that code, command-line tools, distributed source control, and continuous integration tools can support common CAD/CAM collaboration workflows
- **Component publishing:** exploring the idea of a "compiled" component that could be more easily consumed by downstream software

There are many other components that contribute to successful CAD/CAM solutions; however, PEDAL will not explore:

- Building a GUI design suite or IDE or supporting the Language Server Protocol
- Dependency management, beyond basic componentization within a single project
- Exporting geometry to more common formats, beyond what's needed to validate the prototype
